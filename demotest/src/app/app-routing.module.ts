import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeeDataComponent } from './employee-data/employee-data.component';


const routes: Routes = [

  { 
    path: '', 
    component:HomeComponent,
    pathMatch:"full" 
  },

  {
    path:"Home",
    component:HomeComponent,
    
  },

  {
    path:"Employeedetails",
    component:EmployeeDetailsComponent,
    
  },

  {
    path:"Login",
    component:ContactComponent,
    
  },
  {
    path:"EmployeeData",
    component:EmployeeDataComponent,
    
  }

  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
