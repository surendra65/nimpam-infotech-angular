import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(public fb:FormBuilder) { }
formData:FormGroup;

  ngOnInit(): void {
    this.formData=this.fb.group({
      name:["",Validators.required],
      email: ["", [Validators.required, Validators.email]],
      Password:["", Validators.minLength(6)],
      conPass:["",[this.passMatcher.bind(this)]]
    })

  }

  private passMatcher():{[s:string]:boolean}
{
  if(this.formData&& (this.formData.controls.Password.value!==this.formData.controls.conPass.value))
  {
    return{'passMatcher':true}
  }
  else
  {
    return null;
  }
}




click(){
 console.log(this.formData.value);
 alert("successfull")
}


}
