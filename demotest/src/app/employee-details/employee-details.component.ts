import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {


  constructor(public employee:EmployeeService,public fb:FormBuilder) { }
public details;

formData:FormGroup

  ngOnInit(): void {
    this.formData=this.fb.group({
      id:["",Validators.required],
      Name:["",Validators.required],
      Postion:["",Validators.required],
      Salary:["",Validators.required],
    })
   
  }


 
  

  click(){
    if(this.formData.valid)
    {
      this.employee.pempdetails(this.formData.value).subscribe(
        res=>{
          return alert("Added Successfull To Show data Press Get Data buttton")   
         }
      )
    }

  }
}
