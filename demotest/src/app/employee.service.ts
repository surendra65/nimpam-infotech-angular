import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Iemp } from './empinterface';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  public url= "http://localhost:3000/emp";

  constructor(private http:HttpClient) { }

  empdetails():Observable<Iemp[]>{
    return this.http.get<Iemp[]>(this.url);
  }

  pempdetails(a: any):Observable<Iemp[]>{
    return this.http.post<Iemp[]>(this.url,a);
  }

}
