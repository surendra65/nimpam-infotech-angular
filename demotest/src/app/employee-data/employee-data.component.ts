import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-data',
  templateUrl: './employee-data.component.html',
  styleUrls: ['./employee-data.component.css']
})
export class EmployeeDataComponent implements OnInit {

  constructor(private employee:EmployeeService) { }
public details;
  ngOnInit(): void {
    
    this.employee.empdetails()
    .subscribe(data=>this.details=data)
    
  }   
}

